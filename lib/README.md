# Java Client for GeoNames Webservices 

The [Geonames Java library](https://github.com/geonames/webservice-client)
helps you to easily access the [Geonames web services](http://www.geonames.org/export/web-services.html)
with java.

## Links

* http://www.geonames.org/
* http://www.geonames.org/source-code/geonames-1.1.14.jar
