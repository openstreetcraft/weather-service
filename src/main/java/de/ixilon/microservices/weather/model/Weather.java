// Copyright (C) 2018 Gerald Fiedler <gerald@ixilon.de>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package de.ixilon.microservices.weather.model;

import java.util.Optional;

import io.swagger.annotations.ApiModelProperty;

public class Weather {
  
  @ApiModelProperty(notes = "Temperature")
  private Optional<Integer> temperature = Optional.empty();

  @ApiModelProperty(notes = "Precipitation")
  private Optional<Precipitation> precipitation = Optional.empty();

  public Optional<Integer> getTemperature() {
    return temperature;
  }

  public void setTemperature(Integer temperature) {
    this.temperature = Optional.ofNullable(temperature);
  }

  public Optional<Precipitation> getPrecipitation() {
    return precipitation;
  }

  public void setPrecipitation(Precipitation precipitation) {
    this.precipitation = Optional.ofNullable(precipitation);
  }

  public static class Builder {
    private Optional<Integer> temperature = Optional.empty();
    private Optional<Precipitation> precipitation = Optional.empty();

    /**
     * Create a Weather instance.
     */
    public Weather build() {
      Weather weather = new Weather();
      
      weather.temperature = temperature;
      weather.precipitation = precipitation;
      
      return weather;
    }
    
    public Builder withTemperature(double temperature) {
      return withTemperature(((Double) temperature).intValue());
    }
    
    public Builder withTemperature(int temperature) {
      return withTemperature(Optional.of(temperature));
    }
    
    public Builder withTemperature(Optional<Integer> temperature) {
      this.temperature = temperature;
      return this;
    }
    
    public Builder withPrecipitation(Precipitation precipitation) {
      return withPrecipitation(Optional.of(precipitation));
    }
    
    public Builder withPrecipitation(Optional<Precipitation> precipitation) {
      this.precipitation = precipitation;
      return this;
    }
  }
}
