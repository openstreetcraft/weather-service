// Copyright (C) 2018 Gerald Fiedler <gerald@ixilon.de>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package de.ixilon.microservices.weather.controller;

import java.util.Optional;
import java.util.ServiceLoader;

import de.ixilon.microservices.weather.model.Location;
import de.ixilon.microservices.weather.model.Weather;
import de.ixilon.microservices.weather.spi.WeatherProvider;
import de.ixilon.microservices.weather.spi.WeatherProviderException;

public class WeatherService {
  private final ServiceLoader<WeatherProvider> providers;

  public WeatherService() {
    providers = ServiceLoader.load(WeatherProvider.class);
  }

  /**
   * Iterate over all providers and return first result found.
   */
  public Optional<Weather> getWeatherByLocation(Location location) {
    for (WeatherProvider provider : providers) {
      try {
        return Optional.of(provider.getWeatherByLocation(location));
      } catch (WeatherProviderException exception) {
        // try next provider
      }
    }
    return Optional.empty();
  }
  
}
