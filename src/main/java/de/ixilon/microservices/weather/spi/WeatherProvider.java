// Copyright (C) 2018 Gerald Fiedler <gerald@ixilon.de>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package de.ixilon.microservices.weather.spi;

import de.ixilon.microservices.weather.model.Location;
import de.ixilon.microservices.weather.model.Weather;

public interface WeatherProvider {
  /**
   * Find weather information of a given location on the Earth's surface.
   * 
   * @param location location
   * @return weather information
   * @throws WeatherProviderException if location not found
   */
  public Weather getWeatherByLocation(Location location) throws WeatherProviderException;
  
}
