// Copyright (C) 2018 Gerald Fiedler <gerald@ixilon.de>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package de.ixilon.microservices.weather.provider.geonames;

import java.util.Optional;

import org.geonames.WeatherObservation;
import org.geonames.WebService;

import de.ixilon.microservices.weather.model.Location;
import de.ixilon.microservices.weather.model.Precipitation;
import de.ixilon.microservices.weather.model.Weather;
import de.ixilon.microservices.weather.spi.WeatherProvider;
import de.ixilon.microservices.weather.spi.WeatherProviderException;

public class GeonamesWeatherProvider implements WeatherProvider {

  static {
    WebService.setUserName("openstreetcraft");
  }

  @Override
  public Weather getWeatherByLocation(Location location) throws WeatherProviderException {
    try {
      return convert(WebService.findNearByWeather(location.getLatitude(), location.getLongitude()));
    } catch (Exception exception) {
      throw new WeatherProviderException(exception);
    }
  }

  private static Weather convert(WeatherObservation observation) {
    return new Weather.Builder()
        .withTemperature(observation.getTemperature())
        .withPrecipitation(toPrecipitation(observation.getWeatherCondition()))
        .build();
  }

  private static Optional<Precipitation> toPrecipitation(String condition) {
    if (condition.contains("rain")) {
      return Optional.of(Precipitation.RAIN);
    } else if (condition.contains("snow")) {
      return Optional.of(Precipitation.SNOW);
    } else if (condition.contains("thunder")) {
      return Optional.of(Precipitation.THUNDER);
    }
    return Optional.empty();
  }
}
